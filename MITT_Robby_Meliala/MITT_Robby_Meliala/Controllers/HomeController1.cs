﻿using Microsoft.AspNetCore.Mvc;
using MITT_Robby_Meliala.Models;

namespace MITT_Robby_Meliala.Controllers
{
    public class HomeController1 : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Skill()
        {
            return View();
        }
        public IActionResult Skilllevel()
        {
            return View();
        }
        public IActionResult LogOut()
        {
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Create()
        {
            return PartialView("_Create",new SkillViewModel());
        }
        public IActionResult UserProfile()
        {
            return View();
        }
    }
}
