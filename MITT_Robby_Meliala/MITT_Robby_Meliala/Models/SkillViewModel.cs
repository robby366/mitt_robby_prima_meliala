﻿namespace MITT_Robby_Meliala.Models
{
    public class SkillViewModel
    {
        public int SkillID { get; set; }
        public string SkillName { get; set;}
    }
}
